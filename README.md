This is "building gap resolution overnight" repository
for Meanotek submission for AGRR competition

DETAILED INSTALLATION INSTRUCTIONS FOR DEPENDENCIES WILL BE PROVIDED SHORTLY

For now:
To Train model:
1. Convert train data to conll using agrr2conll
2. Run train.py
3. Wait for completion
4. Run predict_gap.py for test file
5. convert data back to agrr format with conll2agrr.py
